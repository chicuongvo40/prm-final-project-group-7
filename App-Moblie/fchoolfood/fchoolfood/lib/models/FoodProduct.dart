class FoodProduct {
  int? pageNumber;
  int? pageSize;
  int? totalNumberOfPages;
  int? totalNumberOfRecords;
  List<Results>? results;

  FoodProduct(
      {this.pageNumber,
        this.pageSize,
        this.totalNumberOfPages,
        this.totalNumberOfRecords,
        this.results});

  FoodProduct.fromJson(Map<String, dynamic> json) {
    pageNumber = json['pageNumber'];
    pageSize = json['pageSize'];
    totalNumberOfPages = json['totalNumberOfPages'];
    totalNumberOfRecords = json['totalNumberOfRecords'];
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results!.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pageNumber'] = this.pageNumber;
    data['pageSize'] = this.pageSize;
    data['totalNumberOfPages'] = this.totalNumberOfPages;
    data['totalNumberOfRecords'] = this.totalNumberOfRecords;
    if (this.results != null) {
      data['results'] = this.results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  int? id;
  String? name;
  String? image;
  int? price;
  String? detail;
  int? status;
  String? createAt;
  String? updatedAt;
  int? categoryId;
  int? quantity;
  int? supplierStoreId;
  String? code;

  Results(
      {this.id,
        this.name,
        this.image,
        this.price,
        this.detail,
        this.status,
        this.createAt,
        this.updatedAt,
        this.categoryId,
        this.quantity,
        this.supplierStoreId,
        this.code});

  Results.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    price = json['price'];
    detail = json['detail'];
    status = json['status'];
    createAt = json['createAt'];
    updatedAt = json['updatedAt'];
    categoryId = json['categoryId'];
    quantity = json['quantity'];
    supplierStoreId = json['supplierStoreId'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    data['price'] = this.price;
    data['detail'] = this.detail;
    data['status'] = this.status;
    data['createAt'] = this.createAt;
    data['updatedAt'] = this.updatedAt;
    data['categoryId'] = this.categoryId;
    data['quantity'] = this.quantity;
    data['supplierStoreId'] = this.supplierStoreId;
    data['code'] = this.code;
    return data;
  }
}