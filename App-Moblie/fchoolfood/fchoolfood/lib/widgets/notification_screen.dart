import 'package:fchoolfood/provider/sign_in_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/postapi.dart';
import 'package:fchoolfood/const/colors.dart';
import 'package:fchoolfood/helper.dart';
import 'package:fchoolfood/customNavBar.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
class NotificationScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    late bool _isSignedIn;
    _isSignedIn = context.read<SignInProvider>().isSignedIn;
    List<Resultss> postData = [];
    final resultProvider = Provider.of<ItemProvider>(context);
    for (var item in resultProvider.item ){
      postData.add(item);
    }
    return Scaffold(
      body: _isSignedIn == true ? Stack(
        children: [
          SafeArea(
              child: Column(
                children: [
                  for (var postData in postData)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child:
                  NotiCard(
                    key: ValueKey('someValue'),
                    title: "Your order has been successed",
                    time: "${postData.checkInDate!.substring(0,10)}  " "  ${postData.checkInDate!.substring(11,19) }",
                  ),
                    )








                ],
              )
          ),
          Positioned(
              bottom: 0,
              left: 0,
              child: CustomNavBar(
                key: ValueKey('someValue'),
                menu: true,
              )
          )
        ],
      ) :
      Stack(
        children: [
          Container(
            height: 900,
          ),
          Positioned(
              bottom: 0,
              left: 0,
              child: CustomNavBar(
                key: ValueKey('someValue'),
                menu: true,
              )
          )
        ],
      )
    );
  }
}

class NotiCard extends StatelessWidget {
  const NotiCard({
    required Key key,
    required String time,
    required String title,
    Color color = Colors.white,
  })  : _time = time,
        _title = title,
        _color = color,
        super(key: key);

  final String _time;
  final String _title;
  final Color _color;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: double.infinity,
      decoration: BoxDecoration(
        color: _color,
        border: Border(
          bottom: BorderSide(
            color: AppColor.placeholder,
            width: 0.5,
          ),
        ),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            backgroundColor: AppColor.orange,
            radius: 5,
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                _title,
                style: TextStyle(
                  color: AppColor.primary,
                ),
              ),
              Text(_time),
            ],
          )
        ],
      ),
    );
  }
}
