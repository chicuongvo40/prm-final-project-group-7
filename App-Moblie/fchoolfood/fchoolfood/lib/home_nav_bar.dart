// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:homepage/profile/profile_screen.dart';
// import 'package:homepage/single_item_page.dart';
// class HomeNavBar extends StatelessWidget {
//   const HomeNavBar({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.symmetric(horizontal: 15),
//       height: 70,
//       decoration: BoxDecoration(
//         color: Colors.deepOrangeAccent.withOpacity(0.5),
//         boxShadow: [
//           BoxShadow(
//             color: Colors.black.withOpacity(0.4),
//             spreadRadius: 1,
//             blurRadius: 8,
//           )
//         ]
//       ),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Icon(
//             Icons.mail,
//             color: Colors.white,
//             size: 35,
//           ),
//           Icon(
//             Icons.favorite_outlined,
//             color: Colors.white,
//             size: 35,
//           ),
//           Container(
//             padding: EdgeInsets.all(15),
//             decoration: BoxDecoration(
//               color: Color(0xFFEFB322),
//               borderRadius: BorderRadius.circular(30),
//                 boxShadow: [
//                   BoxShadow(
//                     color: Colors.white.withOpacity(0.4),
//                     spreadRadius: 1,
//                     blurRadius: 6,
//                   )
//                 ]
//             ),
//             child: Icon(
//               CupertinoIcons.cart_fill,
//               color: Colors.white,
//               size: 35,
//             ),
//           ),
//           Icon(
//             Icons.notifications,
//             color: Colors.white,
//             size: 35,
//           ),
//           IconButton(
//             onPressed: (){
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) => SingleItemPage()),
//               );
//             },
//             icon: Icon(Icons.person),
//             color: Colors.white,
//             iconSize: 35,
//           ),
//         ],
//       ),
//     );
//   }
// }
