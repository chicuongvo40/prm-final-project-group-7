
import 'package:flutter/material.dart';
import 'package:fchoolfood/screens/splash/components/body.dart';
import 'package:fchoolfood/size_config.dart';

import 'dart:async';

class SplashScreen extends StatefulWidget {
  static String routeName = "/splash";
  const SplashScreen({Key? key}) : super(key: key);
  @override
  State<SplashScreen> createState() => _SplashScreenState();}


class _SplashScreenState extends State<SplashScreen> {



  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
