import 'package:flutter/widgets.dart';
import 'package:fchoolfood/screens/splash/splash_screen.dart';
import 'package:fchoolfood/screens/sign_in/sign_in_screen.dart';
import 'package:fchoolfood/screens/login_success/login_success_screen.dart';
import 'package:fchoolfood/screens/forgot_password/forgot_password_screen.dart';
import 'package:fchoolfood/screens/sign_up/sign_up_screen.dart';
import 'package:fchoolfood/screens/home/home_screen.dart';
import 'package:fchoolfood/screens/pratice.dart';
import 'package:fchoolfood/screens/complete_profile/complete_profile_screen.dart';
import 'package:fchoolfood/screens/otp/otp_screen.dart';






// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  MainFoodPage.routeName: (context) => MainFoodPage(),
  OtpScreen.routeName: (context) => OtpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),



};
